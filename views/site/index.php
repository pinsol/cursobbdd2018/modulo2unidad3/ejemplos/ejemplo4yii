<?php
/**
 * precargar las clases
 */
    use yii\helpers\Html;
    
/* @var $this yii\web\View */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Seleccion</h1>

        <p class="lead">Realizando algunas consultas de Selección sobre las tablas emple y depart</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta1"],["class"=>"btn btn-default"]) ?></p>
                
            </div>
            
            <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta2"],["class"=>"btn btn-default"]) ?></p>
                
            </div>
                
            <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta3"],["class"=>"btn btn-default"]) ?></p>
                
            </div>
            <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta4"],["class"=>"btn btn-default"]) ?></p>
               
                
            </div>
            
            <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta5"],["class"=>"btn btn-default"]) ?></p>
                
            </div>
                
            <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta6"],["class"=>"btn btn-default"]) ?></p>
                
            </div>
            
        </div>

    </div>
</div>
