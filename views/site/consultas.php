<?php
use yii\helpers\Html
?>

<div>

<ul>
    <li>
<?= Html::a('Consulta 1', ['emple/consulta1'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 2', ['emple/consulta2'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 3', ['emple/consulta3'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 4', ['emple/consulta4'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 5', ['emple/consulta5'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 6', ['emple/consulta6'], ['class' => 'profile-link']) ?>
    </li>
</ul>
</div>